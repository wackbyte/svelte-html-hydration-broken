# `svelte-html-hydration-broken`

Demonstrates hydrating nested `p` elements with `{@html}` being broken since Svelte 4.0.2.

~~[Here is the relevant issue.](https://github.com/withastro/astro/issues/7557)~~

## Testing

Try Svelte 4.0.1 instead of 4.0.2, and the issue will not occur.

### Running a development server

```sh
$ pnpm dev
```

### Running a production server

```sh
$ pnpm build
$ pnpm preview
```
